<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="blog-index">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <p>
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'text:ntext',
            'url:url',
//            ['attribute' => 'url', 'format' => 'text'],
//            'status_id:boolean',
//            ['attribute' => 'status_id', 'filter' => ['0' => 'off', '1' => 'on'], 'value' => function($model){return $model->status_id;}],
//            ['attribute' => 'status_id', 'filter' => function($model){return $model->getStatusList();}, 'value' => 'statusName'],
            ['attribute' => 'status_id', 'filter' => \yourockwork\blog\models\Blog::STATUS_LIST, 'value' => 'statusName'],
            ['attribute' => 'tags', 'value' => function($model){
                   return implode(',', $model->tags);
                }
            ],
//			['attribute' => 'tags', 'value' => 'tagsAsString'],
            //'sort',
            'smallImage:image',
			'date_create:date',
			'date_update:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{custom} {view} {update} {delete} {check}',
                'buttons' => [
                    'custom' => function($url, $model, $key){
                        return \yii\bootstrap\Html::a('<i class="fa fa-key">Custom</i>', $url);
                    },
					'check' => function($url, $model, $key){
						return \yii\bootstrap\Html::a('<i class="fa fa-check"></i>', $url);
					}
                ],
                'visibleButtons' => [
                    'check' => function ($model, $url, $key){
                        if ($model->status_id === 0){
                            return false;
                        }
                        return true;
                    }
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
