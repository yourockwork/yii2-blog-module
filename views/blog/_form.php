<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model \yourockwork\blog\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'text')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
			'imageUpload' => \yii\helpers\Url::to(['/site/save-redactor-img', 'sub' => 'blog']),
            'formatting' => ['p', 'blockquote', 'h2', 'h1'],
            'plugins' => [
				'clips',
				'fullscreen',
			],
			'clips' => [
				['Lorem ipsum...', 'Lorem...'],
				['red', '<span class="label-red">red</span>'],
				['green', '<span class="label-green">green</span>'],
				['blue', '<span class="label-blue">blue</span>'],
			],
		],
	]) ?>

    <?= $form->field($model, 'tags_array')->widget(\kartik\select2\Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\yourockwork\blog\models\Tag::find()->all(), 'id', 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выбрать тэг', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'tags' => true,
            'maximumInputLength' => 25,
        ]
    ]);

    ?>

    <?=
	    $form->field($model, 'file' /*['options' => ['class' => 'col-xs-6']]*/)->widget(\kartik\file\FileInput::classname(), [
			'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="fa fa-photo"></i>',
                    'browseLabel' => 'Выбрать фото',
            ],
        ]);
	?>

	<?/*=
        \kartik\file\FileInput::widget([
            'name' => 'ImageManager[attachment]',
            'options' => [
                'multiple' => true
            ],
            'pluginOptions' => [
                'uploadUrl' => \yii\helpers\Url::to(['/site/save-img']),
                'uploadExtraData' => [
                    'ImageManager[class]' => $model->formName(),
                    'ImageManager[item_id]' => $model->id,
                ],
                'maxFileCount' => 10
            ],
        ]);
	*/?>

    <?= $form->field($model, 'url' )->textInput(['maxlength' => true]) ?>

    <?/*= $form->field($model, 'status_id')->dropDownList(['0' => 'off', '1' => 'on']) */?>
    <?= $form->field($model, 'status_id')->dropDownList(\yourockwork\blog\models\Blog::STATUS_LIST) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>