<?php

namespace yourockwork\blog\models;

use common\models\User;
use Yii;
use yii\helpers\Url;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\image\drivers\Image;
use yii\behaviors\TimestampBehavior;
use common\components\behaviors\StatusBehavior;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $url
 * @property string $image
 * @property int $status_id
 * @property int $sort
 * @property datetime date_create
 * @property datetime date_update
 */
class Blog extends \yii\db\ActiveRecord
{
	const STATUS_LIST = ['off', 'on'];
	public $tags_array;
	public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['text'], 'string'],
            [['status_id'], 'integer'],
			[['sort'], 'integer', 'max' => 99, 'min' => 1],
            [['title'], 'string', 'max' => 150],
            [['image'], 'string', 'max' => 100],
            [['url'], 'unique'],
            [['file'], 'image'],
            [['url'], 'string', 'max' => 255],
            [['tags_array'], 'safe'],
            [['date_update'], 'safe'],
            [['date_create'], 'safe'],
        ];
    }

	public function behaviors()
	{
		return [
			'timstampBehavior' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
				],
				// если вместо метки времени UNIX используется datetime:
				'value' => new Expression('NOW()'),
			],
			'statusBehavior' => [
				'class' => StatusBehavior::className(),
				'statusList' => self::STATUS_LIST,
			]
		];
	}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'url' => 'Url',
            'status_id' => 'Статус ID',
            'sort' => 'Сортировка',
            'tags_array' => 'Теги',
            'image' => 'Картинка',
            'file' => 'Картинка',
			'tagsAsString' => 'Тэги',
			'author.username' => 'Автор',
			'author.email' => 'Почта',
			'date_create' => 'Дата создания',
			'date_update' => 'Дата обновления',
        ];
    }

	public function getBlogTag(){
		return $this->hasMany(BlogTag::className(), ['blog_id' => 'id']);
	}

    public function getAuthor(){
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getTags()
	{
		// не работает, хотя должно. Наверное проблема в имени свзи `blogTag`
		//return $this->hasMany(Tag::className(), ['id', 'tag_id'])->via('blogTag');

		return $this->hasMany(Tag::className(), ['id', 'tag_id'])->viaTable('blog_tag', ['tag_id' => 'id']);
	}

	public function getTagsAsString()
	{
		$arr = ArrayHelper::map($this->tags, 'id', 'name');

		return implode(',' , $arr);
	}

	public function afterFind()
	{
		parent::afterFind();
		$this->tags_array = $this->tags;
	}

	public function afterTagsAsString()
	{
		$arr = ArrayHelper::map($this->tags, 'id', 'name');

		return implode(',', $arr);
	}

	public function afterSave($insert, $changedAttributes){

		return parent::afterSave($insert, $changedAttributes);

		$arr = ArrayHelper::map($this->tags, 'id', 'id');
		foreach ($this->tags_array as $one){
			if (!in_array($one, $arr)){
				$model = new BlogTag();
				$model->blog_id = $this->id;
				$model->tag_id = $one;
				$model->save();
			}
			if (isset($arr[$one])){
				unset($arr[$one]);
			}
		}
		BlogTag::deleteAll(['tag_id' => $arr]);
	}

	public function beforeSave($insert){
		if ($file = UploadedFile::getInstance($this, 'file')){

			$dir = Yii::getAlias('@images') . '/blog/';

			if (file_exists($dir . $this->image)){
				unlink($dir . $this->image);
			}

			if (file_exists($dir.'50x50/'.$this->image)){
				unlink($dir.'50x50/'.$this->image);
			}

			if (file_exists($dir.'800x/'.$this->image)){
				unlink($dir.'800x/'.$this->image);
			}

			$this->image = strtotime('now') . '_' . Yii::$app->getSecurity()->generateRandomString(6) . '.' . $file->extension;

			$file->saveAs($dir.$this->image);

			$imag = Yii::$app->image->load($dir.$this->image);
			$imag->background('#fff', 0);
			$imag->resize('50', '50', Image::INVERSE);
			$imag->crop('50', '50');
			$imag->save($dir.'50x50/'.$this->image, 90);
			$imag = Yii::$app->image->load($dir.$this->image);
			$imag->background('#fff', 0);
			$imag->resize('800', null, Image::INVERSE);
			$imag->save($dir.'800x/'.$this->image, 90);
		}

		return parent::beforeSave($insert);
	}

	public function getSmallImage(){
		if ($this->image){
			$path = str_replace('admin', '', Url::home(true)).'uploads/images/blog/' . '50x50/' . $this->image;
		}
		else{
			$path = str_replace('admin', '', Url::home(true)).'uploads/images/no-img.png';
		}

		return $path;
	}
}