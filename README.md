Blog for yii2. Не поддерживается
================================
Blog for yii2. Не поддерживается

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist yourockwork/yii2-blog-module "*"
```

or add

```
"yourockwork/yii2-blog-module": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \yourockwork\blog\AutoloadExample::widget(); ?>```